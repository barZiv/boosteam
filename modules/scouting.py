from operator import *

def calcScore(team):
    team['score'] += (int(team["mineralsInHigh"])*5)
    team['score'] += (int(team["mineralsInSquare"])*2)
    team['score'] += team['land']*30
    team['score'] += team['touch']*10
    team['score'] += team['mineral']*25
    team['score'] += team['kamea']*15
    team['score'] += team['on']*15
    team['score'] += team['in']*25
    team['score'] += team['hang']*50

def calcPoints(teamDict, number, land, touch, kamea, mineral, mineralsInSquare, mineralsInHigh, endGame):
    if (number in teamDict):
        teamDict[number]["mineralsInHigh"] = str(int(teamDict[number]["mineralsInHigh"]) + int(mineralsInHigh))
        teamDict[number]["mineralsInSquare"] = str(int(teamDict[number]["mineralsInSquare"]) + int(mineralsInSquare))
        if land:
            teamDict[number]['land'] += 1
        if touch:
            teamDict[number]["touch"] += 1
        if mineral:
            teamDict[number]["mineral"] += 1
        if kamea:
            teamDict[number]["kamea"] += 1

        if endGame == "on":
            teamDict[number]["on"] += 1
        elif endGame == "in":
            teamDict[number]["in"] += 1
        elif endGame == "hang":
            teamDict[number]["hang"] += 1
    else:
        teamDict[number] = {"mineralsInSquare": mineralsInSquare, "mineralsInHigh": mineralsInHigh}
        if land:
            teamDict[number]['land'] = 1
        else:
            teamDict[number]['land'] = 0
        if touch:
            teamDict[number]["touch"] = 1
        else:
            teamDict[number]["touch"] = 0
        if mineral:
            teamDict[number]["mineral"] = 1
        else:
            teamDict[number]["mineral"] = 0
        if kamea:
            teamDict[number]["kamea"] = 1
        else:
            teamDict[number]["kamea"] = 0

        if endGame == "on":
            teamDict[number]["on"] = 1
            teamDict[number]["in"] = 0
            teamDict[number]["hang"] = 0
        elif endGame == "in":
            teamDict[number]["in"] = 1
            teamDict[number]["on"] = 0
            teamDict[number]["hang"] = 0
        elif endGame == "hang":
            teamDict[number]["in"] = 0
            teamDict[number]["on"] = 0
            teamDict[number]["hang"] = 1
        else:
            teamDict[number]["in"] = 0
            teamDict[number]["on"] = 0
            teamDict[number]["hang"] = 0
    teamDict[number]["score"] = 0
    calcScore(teamDict[number])
    return sort(teamDict)

def sort(teamDict):
    l = []
    s = []
    for key in teamDict:
        l.append(key)
        s.append(teamDict[key]['score'])

    n = len(s)
    # Traverse through all array elements
    for i in range(n):
        # Last i elements are already in place
        for j in range(0, n-i-1):
            # traverse the array from 0 to n-i-1
            # Swap if the element found is greater
            # than the next element
            if s[j] < s[j+1] :
                s[j], s[j+1] = s[j+1], s[j]
                l[j], l[j+1] = l[j+1], l[j]
    return organizedData(teamDict, l)

def organizedData(teamDict, l):
    data = ''
    for team in l:
        data += '''
        <tr>
            <th style="width: 5%"> {} </th>
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
                
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
                
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
            <th style="width: 10%">{}</th>
            <th style="width: 5%">{}</th>
        </tr>
        '''.format(team, teamDict[team]['land'], teamDict[team]['mineral'], teamDict[team]['kamea'], teamDict[team]['touch'], teamDict[team]['mineralsInSquare'], teamDict[team]['mineralsInHigh'], teamDict[team]['on'], teamDict[team]['in'], teamDict[team]['hang'], teamDict[team]['score'])
    return data
